# Self Study

Here I log my journey as I read through several textbooks (and other resources) in my endeavour of enjoying maths and computer science.

<a name="toc"></a>

## Table of Contents

- [Mathematics](#mathematics)

    - [Precalculus](#precalculus)


## Mathematics

### Precalculus

[<sup>Back to top</sup>](#toc)

#### Books

- **George F. Simmons.** Precalculus Mathematics in a Nutshell: Geometry, Algebra, Trigonometry **(1 ed.)**

    **Status:** all exercises solved.  
    **Started:** 07-Jan-2019  
    **Completed:** 11-Jun-2019  
    **Notes:** despite being an introduction to geometry, algebra a trigonometry (a.k.a basic high school level stuff), it felt hard as it was the first math book I've ever read completely and in a disciplined manner. Finishing it was quite satisfying as it showed me that I can handle math heavy content and set me up for the upcoming material.

- **Edwin Beckenbach, Richard Bellman.** An Introduction to Inequalities **(1 ed.)**

    **Status:** all exercises solved.  
    **Started:** 26-Feb-2020  
    **Completed:** 23-Sep-2020  
    **Notes:** fantastic book for learning inequalities, from the basic ones to those used in higher courses like analysis, all while maintaining an approachable language (no sigma notation for summations, even). Remarks to the proof technique of backward induction, and the relation between the four main types of means (average, geometric, harmonic, root-mean-square) and their placement on a trapezoid.

- **Serge Lang, Gene Murrow.** Geometry: A High School Course **(2 ed.)**

    **Status:** all exercises solved.  
    **Started:** 25-Seb-2020  
    **Completed:** 19-Nov-2020  
    **Notes:** incredible book on basic geometry. What jumped out to me the most was the rigorous way in which it presented the topics, resembling higher level books: axioms, definitions and theorems followed by their proofs. Which is a really good way to introduce the reader to the structure higher maths books have. The last chapters on transformations and isomorphisms also served as a good introduction to abstract mathematics. Compass, ruler and protractor are a must as the constructions presented are quite enlightening.

- **Kenneth S. Miller, John B. Walsh.** Elementary and Advanced Trigonometry **(1 ed.)**

    **Status:** most exercises solved. Only did 5~10 exercises on trigonometric identities, skipped the exercises about static analysis (calculating forces in structures like bridges) and skimmed the last two chapters (see notes).  
    **Started:** 30-Nov-2020  
    **Completed:** 29-Mar-2021  
    **Notes:** more than a complete book for learning trigonometry, dipping into complex numbers, calculus, hyperbolic functions, Fourier series and Tschebyscheff polynomials. The book is a bit dated as evidence by the section on the use of the slide rule for computing logarithms but it can be skipped entirely without losing anything. Exercises were tough, specially in the advanced part, and even more specially the section about limits (the epsilon-delta definition of limit was the most difficult thing I encountered). The authors recognized this and properly warned at the beginning of the chapter as a proper course on calculus was out of scope for the book. Nonetheless, the advanced topics were absolutely rewarding and served as a great exposure into higher level math. The last two chapters (Fouries series and Tschebyscheff polynomials) were only skimmed as I reckoned those topics to be more suitable in a course of calculus/analysis and numerical analysis. Remarks to the hyperbolic functions for such elegant extension of the trigonometric functions into the complex numbers.
