# Path to Wisdom

The mere purpose of this repository is to serve as a memory well to archive interesting resources that I find laying somewhere and recall them in the future.

### Enactments of the processes

#### Storing a memory

<img src="storing.gif" alt="Storing" height="350px"/>

#### Recalling a memory

<img src="recalling.gif" alt="Recalling" height="300px"/>

*Note: Pensieve, actors, wands, spells or hats are not included.*