# Project Development

This compendium tracks resources related to project development practices and my thoughts on them.

<a name="toc"></a>

----

## Table of Contents

- [Design](#design)
- [General Practices](#general-practices)

----

## Design

[<sup>Back to top</sup>](#toc)

- Painless Functional Specifications

  ​	[Part 1: Why Bother?](https://www.joelonsoftware.com/2000/10/02/painless-functional-specifications-part-1-why-bother/)

  ​	[Part 2: What’s a Spec?](https://www.joelonsoftware.com/2000/10/03/painless-functional-specifications-part-2-whats-a-spec/)

  ​	[Part 3: But… How?](https://www.joelonsoftware.com/2000/10/04/painless-functional-specifications-part-3-but-how/)

  ​	[Part 4: Tips](https://www.joelonsoftware.com/2000/10/15/painless-functional-specifications-part-4-tips/)

  **Author(s):** Joel Spolsky

  A functional spec is part of the design process and defines WHAT the product/project/program must achieve. The HOW part is delegated to the technical spec.

  *Note: from now on I will refer to the functional spec also as the design document.*

  The functional spec must detailed everything of what must be done and everybody has to agree on what's written in it, so all the involved parts have to contribute to the functional/design document, hence, playing part in the design process. Although, a single person must be responsible for the document itself.

  Don't forget to keep the design document (and others too!) as simple as possible, funny, well written and adaptable.

- [How to write a good software design doc](https://medium.freecodecamp.org/how-to-write-a-good-software-design-document-66fcf019569c)

  **Author(s):** Angela Zhang

  This is a good structure for a simple design document, nothing fancy or really technical, just to get the right things done. It can be adapted depending on the project.

- [How to effectively scope your software projects](https://medium.freecodecamp.org/how-to-effectively-scope-your-software-projects-from-planning-to-execution-e96cbcac54b9)

  **Author(s):** Angela Zhang

  Some simple guidelines and tips to keep in mind when designing and planning a project, specially the part of de-risking the project by identifying the choke points and addressing them first (e.g., developing them first or look for other solutions).

- [What is the difference between technical specifications and design documents?](https://softwareengineering.stackexchange.com/questions/179554/what-is-the-difference-between-technical-specifications-and-design-documents)

  It's good to remember that the design document and the technical spec are two different things, the first being a high level definition of the product/project and taking care of the WHAT part of the designing process, and the second providing the specifics on HOW to achieve that (e.g., programming languages, tools, data structures, algorithms, etc.). I merge them in a single document when the project isn't highly complex (in their respective sections), otherwise I prefer them in different files.

## General Practices

[<sup>Back to top</sup>](#toc)

- [The Joel Test: 12 Steps to Better Code](https://www.joelonsoftware.com/2000/08/09/the-joel-test-12-steps-to-better-code/)

  **Author(s):** Joel Spolsky

  Twelve simple questions to keep in mind when developing any software project. In order to have a really good quality end product, you MUST answer yes to ALL of them:

  > 1. Do you use source control?
  > 2. Can you make a build in one step?
  > 3. Do you make daily builds?
  > 4. Do you have a bug database?
  > 5. Do you fix bugs before writing new code?
  > 6. Do you have an up-to-date schedule?
  > 7. Do you have a spec?
  > 8. Do programmers have quiet working conditions?
  > 9. Do you use the best tools money can buy?
  > 10. Do you have testers?
  > 11. Do new candidates write code during their interview?
  > 12. Do you do hallway usability testing?

  *Note: this by no means will guarantee a flawless project, its goal, instead, is to make it easy to remember some minimum good practices.